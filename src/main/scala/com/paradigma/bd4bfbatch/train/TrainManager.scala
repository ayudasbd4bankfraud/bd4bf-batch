package com.paradigma.bd4bfbatch.train

import com.datastax.spark.connector._
import com.datastax.spark.connector.rdd.CassandraTableScanRDD
import com.paradigma.bd4bfbatch.domain.{BD4BFModel, BD4BFStandardScaler, TransactionFeatures}
import com.paradigma.bd4bfbatch.util.BD4BFBatchUtils
import com.paradigma.bd4bfbatch.util.BD4BFBatchUtils._
import org.apache.log4j.{Level, Logger}
import org.apache.spark.mllib.classification.{LogisticRegressionModel, LogisticRegressionWithSGD}
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.feature.{StandardScaler, StandardScalerModel}
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.mutable

/**
  * Created by paradigma
  */
object TrainManager {

  //private val master = "local[*]"
  private val appName = "BD4BFBatchTrainManager"
  private val appId = "BD4BFBatchTrainManager"

  def train(): Unit = {

    Logger.getLogger("org").setLevel(Level.ERROR)

    val sc: SparkContext = new SparkContext(new SparkConf()
      .setMaster(BD4BFBatchUtils.master)
      .setAppName(appName)
      .set("spark.app.id", appId)
      .set("spark.cassandra.connection.host", cassandraHost))

    // read all txns from txn_features column family
    val transactionsWithFeaturesFromCassandra: CassandraTableScanRDD[TransactionFeatures] =
      sc.cassandraTable[TransactionFeatures](BD4BFBatchUtils.cassandraKeyspace, BD4BFBatchUtils.cassandraFeatureTable)

    if (transactionsWithFeaturesFromCassandra.isEmpty) {
      println("No transactions for training process, impossible to continue...")
      throw new RuntimeException("No transactions for training process, impossible to continue")
    }

    val featureVectors: RDD[(String, Int, Vector)] =
      fromRddToVector(transactionsWithFeaturesFromCassandra)

    val standardScalerModel: StandardScalerModel =
      new StandardScaler().fit(featureVectors.map({ case (transactionId, fraud, features) => features }))

    val dataForTraining: RDD[LabeledPoint] =
      featureVectors.map({
        case (transactionId, fraud, features) => new LabeledPoint(fraud.toDouble, standardScalerModel.transform(features))
      }).cache

    val iterations: Int = BD4BFBatchUtils.trainingIterations
    val stepSize: Int = BD4BFBatchUtils.trainingStepSize
    val logisticRegressionModel: LogisticRegressionModel =
      LogisticRegressionWithSGD.train(dataForTraining, iterations, stepSize)

    // save model to C*
    sc.parallelize(Seq(new BD4BFModel(
      System.currentTimeMillis(),
      logisticRegressionModel.weights.toArray.toList,
      logisticRegressionModel.intercept,
      logisticRegressionModel.numFeatures,
      logisticRegressionModel.numClasses))).saveToCassandra(cassandraKeyspace, BD4BFBatchUtils.cassandraLRModelTable,
      BD4BFBatchUtils.lrModelColumnSelector)

    // save standardScaler to C*
    sc.parallelize(
      Seq(new BD4BFStandardScaler(
        System.currentTimeMillis(),
        standardScalerModel.std.toArray.toList,
        standardScalerModel.mean.toArray.toList))).saveToCassandra(BD4BFBatchUtils.cassandraKeyspace,
      BD4BFBatchUtils.cassandraSSCModelTable, sscModelColumnSelector)

    // MODEL VALIDATION PROCESS
    validateModel(transactionsWithFeaturesFromCassandra, sc)

    // STOP SPARK CONTEXT
    sc.stop
  }

  private def validateModel(transactions: CassandraTableScanRDD[TransactionFeatures], sc: SparkContext): Unit = {
    val scaler: StandardScaler = new StandardScaler()

    val legalTxns: RDD[TransactionFeatures] = transactions.filter(txn => txn.fraud == 0)
    val legalTxnSplits: Array[RDD[TransactionFeatures]] = legalTxns.randomSplit(Array(0.7, 0.3), System.currentTimeMillis)
    val fraudTxns = transactions.filter(txn => txn.fraud == 1)
    val fraudTxnSplits: Array[RDD[TransactionFeatures]] = fraudTxns.randomSplit(Array(0.7, 0.3), System.currentTimeMillis)

    // TRAIN DATA
    val legalTrainTxns: RDD[TransactionFeatures] = legalTxnSplits(0)
    val fraudTrainTxns: RDD[TransactionFeatures] = fraudTxnSplits(0)
    val trainingData: RDD[TransactionFeatures] = legalTrainTxns.union(fraudTrainTxns)

    val featureVectorsTrain: RDD[(String, Int, Vector)] = fromRddToVector(trainingData)

    val scalerModel: StandardScalerModel =
      scaler.fit(featureVectorsTrain.map({ case ((txnid, fraud, features)) => features }))

    val dataForTraining: RDD[LabeledPoint] =
      featureVectorsTrain.map({
        case ((txnid, fraud, features)) => new LabeledPoint(fraud.toDouble, scalerModel.transform(features))
      }).cache

    val iterations: Int = 5000
    val stepSize: Int = 500
    val logisticRegressionModel: LogisticRegressionModel =
      LogisticRegressionWithSGD.train(dataForTraining, iterations, stepSize)

    // VALIDATE DATA
    val legalValidateTxns: RDD[TransactionFeatures] = legalTxnSplits(1)
    val fraudValidateTxns: RDD[TransactionFeatures] = fraudTxnSplits(1)
    val validationData: RDD[TransactionFeatures] = legalValidateTxns.union(fraudValidateTxns)

    val featureVectorsValidate: RDD[(String, Int, Vector)] = fromRddToVector(validationData)

    val dataForValidating: RDD[(String, Int, Vector)] =
      featureVectorsValidate.map({
        case ((txnid, fraud, features)) => (txnid, fraud, scalerModel.transform {
          features
        })
      })

    // Compute raw scores on the test set
    val predictionAndLabels: RDD[(String, Int, Double)] = dataForValidating.map { case ((txnid, fraud, features)) =>
      val prediction = logisticRegressionModel.predict(features)
      (txnid, fraud, prediction)
    }

    // Instantiate metrics object
    val metrics = new BinaryClassificationMetrics(predictionAndLabels.map({
      case ((txnid, fraud, prediction)) => (prediction, fraud.toDouble)
    }))

    // Precision by threshold
    val precision = metrics.precisionByThreshold
    precision.foreach { case (t, p) =>
      println(s"Threshold: $t, Precision: $p")
    }

    println("areaUnderROC: " + metrics.areaUnderROC())
    println("Model weights: " + logisticRegressionModel.weights)
    println("Model intercept: " + logisticRegressionModel.intercept)


    val modelWeigths = logisticRegressionModel.weights.toArray
    val weights = mutable.Map.empty[String, mutable.Map[String, String]]
    weights.put("amount", mutable.Map[String, String](("value", modelWeigths(0).toString)))
    weights.put("amountOverMonth", mutable.Map[String, String](("value", modelWeigths(1).toString)))
    weights.put("amountSameDay", mutable.Map[String, String](("value", modelWeigths(2).toString)))
    weights.put("averageDailyOverMonth", mutable.Map[String, String](("value", modelWeigths(3).toString)))
    weights.put("amountMerchantTypeOverMonth", mutable.Map[String, String](
      (Merchants.head, modelWeigths(4).toString),
      (Merchants(1), modelWeigths(5).toString),
      (Merchants(2), modelWeigths(6).toString),
      (Merchants(3), modelWeigths(7).toString)))
    weights.put("numberMerchantTypeOverMonth", mutable.Map[String, String](
      (Merchants.head, modelWeigths(8).toString),
      (Merchants(1), modelWeigths(9).toString),
      (Merchants(2), modelWeigths(10).toString),
      (Merchants(3), modelWeigths(11).toString)))
    weights.put("amountSameCountryOverMonth", mutable.Map[String, String](
      (Countries.head, modelWeigths(12).toString),
      (Countries(1), modelWeigths(13).toString),
      (Countries(2), modelWeigths(14).toString),
      (Countries(3), modelWeigths(15).toString),
      (Countries(4), modelWeigths(16).toString),
      (Countries(5), modelWeigths(17).toString)))
    weights.put("numberSameCountryOverMonth", mutable.Map[String, String](
      (Countries.head, modelWeigths(18).toString),
      (Countries(1), modelWeigths(19).toString),
      (Countries(2), modelWeigths(20).toString),
      (Countries(3), modelWeigths(21).toString),
      (Countries(4), modelWeigths(22).toString),
      (Countries(5), modelWeigths(23).toString)))
    weights.put("averageOverThreeMonths", mutable.Map[String, String](("value", modelWeigths(24).toString)))
    weights.put("ecommerce", mutable.Map[String, String](("value", modelWeigths(25).toString)))
    weights.put("foreign", mutable.Map[String, String](("value", modelWeigths(26).toString)))
    weights.put("numberSameDay", mutable.Map[String, String](("value", modelWeigths(27).toString)))
    weights.put("amountMerchantTypeOverThreeMonths", mutable.Map[String, String](
      (Merchants.head, modelWeigths(28).toString),
      (Merchants(1), modelWeigths(29).toString),
      (Merchants(2), modelWeigths(30).toString),
      (Merchants(3), modelWeigths(31).toString)))

    // TRUE POSITIVE
    val truePositive: Double = predictionAndLabels.filter({
      case ((txnid, fraud, prediction)) => fraud == 1 && prediction == 1.0
    }).count
    println(truePositive)

    // TRUE NEGATIVE
    val trueNegative: Double = predictionAndLabels.filter({
      case ((txnid, fraud, prediction)) => fraud == 0 && prediction == 0.0
    }).count
    println(trueNegative)

    // FALSE POSITIVE
    val falsePositive: Double = predictionAndLabels.filter({
      case ((txnid, fraud, prediction)) => fraud == 1 && prediction == 0.0
    }).count
    println(falsePositive)

    // FALSE NEGATIVE
    val falseNegative: Double = predictionAndLabels.filter({
      case ((txnid, fraud, prediction)) => fraud == 0 && prediction == 1.0
    }).count
    println(falseNegative)

    // sensitivity
    val sensitivity: Double = truePositive / (truePositive + falseNegative)

    // specificity
    val specificity: Double = trueNegative / (falsePositive + trueNegative)

    sc.parallelize(Seq((System.currentTimeMillis, weights, metrics.areaUnderROC(), specificity, sensitivity))).
      saveToCassandra("bd4bf", "model_features",
        SomeColumns("date", "weights", "area_under_roc", "specificity", "sensitivity"))

  }

}
