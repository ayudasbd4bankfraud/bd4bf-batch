package com.paradigma.bd4bfbatch.statistics

import com.datastax.spark.connector._
import com.datastax.spark.connector.rdd.CassandraTableScanRDD
import com.paradigma.bd4bfbatch.domain.TransactionFeatures
import org.apache.spark.mllib.feature.StandardScaler
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by paradigma
  */
object StatisticsManager {

  private val master = "local[*]"
  private val appName = "BD4BFBatchStatisticsManager"
  private val appId = "BD4BFBatchStatisticsManager"

  val Merchants = List[String]("0742", "0763", "1520", "0780")
  val Countries = List[String]("FR", "US", "ES", "GB", "GR", "CH" )

  def statistics(): Unit = {

    val sc: SparkContext = new SparkContext(new SparkConf()
      .setMaster(master)
      .setAppName(appName)
      .set("spark.app.id", appId)
      .set("spark.cassandra.connection.host", "localhost"))

    // transactions with features from C*
    val features: CassandraTableScanRDD[TransactionFeatures] =
      sc.cassandraTable[TransactionFeatures]("bd4bf", "anonymized_txn_features")

    val legal = features.filter(txn => txn.fraud == 0).cache
    val fraud = features.filter(txn => txn.fraud == 1).cache

    // LEGAL
    val legalAmount: RDD[Double] = legal.map(txn => txn.amount)
    val legalAmountMean = legalAmount.mean
    val legalAmountStdev = legalAmount.stdev
    val legalAmountMin = legalAmount.min
    val legalAmountMax = legalAmount.max

    val legalAmountOverMonth = legal.map(txn => txn.amountOverMonth)
    val legalAmountOverMonthMean = legalAmountOverMonth.mean
    val legalAmountOverMonthStdev = legalAmountOverMonth.stdev
    val legalAmountOverMonthMin = legalAmountOverMonth.min
    val legalAmountOverMonthMax = legalAmountOverMonth.max

    val legalAmountSameDay = legal.map(txn => txn.amountSameDay)
    val legalAmountSameDayMean = legalAmountSameDay.mean
    val legalAmountSameDayStdev = legalAmountSameDay.stdev
    val legalAmountSameDayMin = legalAmountSameDay.min
    val legalAmountSameDayMax = legalAmountSameDay.max

    val legalAverageDailyOverMonth = legal.map(txn => txn.averageDailyOverMonth)
    val legalAverageDailyOverMonthMean = legalAverageDailyOverMonth.mean
    val legalAverageDailyOverMonthStdev = legalAverageDailyOverMonth.stdev
    val legalAverageDailyOverMonthMin = legalAverageDailyOverMonth.min
    val legalAverageDailyOverMonthMax = legalAverageDailyOverMonth.max

    val legalAverageOverThreeMonths = legal.map(txn => txn.averageOverThreeMonths)
    val legalAverageOverThreeMonthsMean = legalAverageOverThreeMonths.mean
    val legalAverageOverThreeMonthsStdev = legalAverageOverThreeMonths.stdev
    val legalAverageOverThreeMonthsMin = legalAverageOverThreeMonths.min
    val legalAverageOverThreeMonthsMax = legalAverageOverThreeMonths.max

    val legalEcommerce = legal.map(txn => txn.ecommerce.toDouble)
    val legalEcommerceMean = legalEcommerce.mean
    val legalEcommerceStdev = legalEcommerce.stdev
    val legalEcommerceMin = legalEcommerce.min
    val legalEcommerceMax = legalEcommerce.max

    val legalForeign = legal.map(txn => txn.foreign.toDouble)
    val legalForeignMean = legalForeign.mean
    val legalForeignStdev = legalForeign.stdev
    val legalForeignMin = legalForeign.min
    val legalForeignMax = legalForeign.max

    val legalNumberSameDay = legal.map(txn => txn.numberSameDay.toDouble)
    val legalNumberSameDayMean = legalNumberSameDay.mean
    val legalNumberSameDayStdev = legalNumberSameDay.stdev
    val legalNumberSameDayMin = legalNumberSameDay.min
    val legalNumberSameDayMax = legalNumberSameDay.max

    // FRAUD
    val fraudAmount: RDD[Double] = fraud.map(txn => txn.amount)
    val fraudAmountMean = fraudAmount.mean
    val fraudAmountStdev = fraudAmount.stdev
    val fraudAmountMin = fraudAmount.min
    val fraudAmountMax = fraudAmount.max

    val fraudAmountOverMonth = fraud.map(txn => txn.amountOverMonth)
    val fraudAmountOverMonthMean = fraudAmountOverMonth.mean
    val fraudAmountOverMonthStdev = fraudAmountOverMonth.stdev
    val fraudAmountOverMonthMin = fraudAmountOverMonth.min
    val fraudAmountOverMonthMax = fraudAmountOverMonth.max

    val fraudAmountSameDay = fraud.map(txn => txn.amountSameDay)
    val fraudAmountSameDayMean = fraudAmountSameDay.mean
    val fraudAmountSameDayStdev = fraudAmountSameDay.stdev
    val fraudAmountSameDayMin = fraudAmountSameDay.min
    val fraudAmountSameDayMax = fraudAmountSameDay.max

    val fraudAverageDailyOverMonth = fraud.map(txn => txn.averageDailyOverMonth)
    val fraudAverageDailyOverMonthMean = fraudAverageDailyOverMonth.mean
    val fraudAverageDailyOverMonthStdev = fraudAverageDailyOverMonth.stdev
    val fraudAverageDailyOverMonthMin = fraudAverageDailyOverMonth.min
    val fraudAverageDailyOverMonthMax = fraudAverageDailyOverMonth.max

    val fraudAverageOverThreeMonths = fraud.map(txn => txn.averageOverThreeMonths)
    val fraudAverageOverThreeMonthsMean = fraudAverageOverThreeMonths.mean
    val fraudAverageOverThreeMonthsStdev = fraudAverageOverThreeMonths.stdev
    val fraudAverageOverThreeMonthsMin = fraudAverageOverThreeMonths.min
    val fraudAverageOverThreeMonthsMax = fraudAverageOverThreeMonths.max

    val fraudEcommerce = fraud.map(txn => txn.ecommerce.toDouble)
    val fraudEcommerceMean = fraudEcommerce.mean
    val fraudEcommerceStdev = fraudEcommerce.stdev
    val fraudEcommerceMin = fraudEcommerce.min
    val fraudEcommerceMax = fraudEcommerce.max

    val fraudForeign = fraud.map(txn => txn.foreign.toDouble)
    val fraudForeignMean = fraudForeign.mean
    val fraudForeignStdev = fraudForeign.stdev
    val fraudForeignMin = fraudForeign.min
    val fraudForeignMax = fraudForeign.max

    val fraudNumberSameDay = fraud.map(txn => txn.numberSameDay.toDouble)
    val fraudNumberSameDayMean = fraudNumberSameDay.mean
    val fraudNumberSameDayStdev = fraudNumberSameDay.stdev
    val fraudNumberSameDayMin = fraudNumberSameDay.min
    val fraudNumberSameDayMax = fraudNumberSameDay.max

    // NORMALIZED

    val featureVectors: RDD[Vector] = features.map(txn => Vectors.dense(Array(
      txn.amount,
      txn.amountOverMonth,
      txn.amountSameDay,
      txn.averageDailyOverMonth,
      txn.averageOverThreeMonths,
      txn.ecommerce.toDouble,
      txn.foreign.toDouble,
      txn.numberSameDay.toDouble)))

    val scaler: StandardScaler = new StandardScaler()
    val scalerModel = scaler.fit(featureVectors)
    val normalize: RDD[Vector] = scalerModel.transform(featureVectors)

    val normalizedAmount = normalize.map(txn => txn(0))
    val normalizedAmountMean = normalizedAmount.mean
    val normalizedAmountStdev = normalizedAmount.stdev
    val normalizedAmountMin = normalizedAmount.min
    val normalizedAmountMax = normalizedAmount.max

    val normalizedAmountOverMonth = normalize.map(txn => txn(1))
    val normalizedAmountOverMonthMean = normalizedAmountOverMonth.mean
    val normalizedAmountOverMonthStdev = normalizedAmountOverMonth.stdev
    val normalizedAmountOverMonthMin = normalizedAmountOverMonth.min
    val normalizedAmountOverMonthMax = normalizedAmountOverMonth.max

    val normalizedAmountSameDay = normalize.map(txn => txn(2))
    val normalizedAmountSameDayMean = normalizedAmountSameDay.mean
    val normalizedAmountSameDayStdev = normalizedAmountSameDay.stdev
    val normalizedAmountSameDayMin = normalizedAmountSameDay.min
    val normalizedAmountSameDayMax = normalizedAmountSameDay.max

    val normalizedAverageDailyOverMonth = normalize.map(txn => txn(3))
    val normalizedAverageDailyOverMonthMean = normalizedAverageDailyOverMonth.mean
    val normalizedAverageDailyOverMonthStdev = normalizedAverageDailyOverMonth.stdev
    val normalizedAverageDailyOverMonthMin = normalizedAverageDailyOverMonth.min
    val normalizedAverageDailyOverMonthMax = normalizedAverageDailyOverMonth.max

    val normalizedAverageOverThreeMonths = normalize.map(txn => txn(4))
    val normalizedAverageOverThreeMonthsMean = normalizedAverageOverThreeMonths.mean
    val normalizedAverageOverThreeMonthsStdev = normalizedAverageOverThreeMonths.stdev
    val normalizedAverageOverThreeMonthsMin = normalizedAverageOverThreeMonths.min
    val normalizedAverageOverThreeMonthsMax = normalizedAverageOverThreeMonths.max

    val normalizedEcommerce = normalize.map(txn => txn(5))
    val normalizedEcommerceMean = normalizedEcommerce.mean
    val normalizedEcommerceStdev = normalizedEcommerce.stdev
    val normalizedEcommerceMin = normalizedEcommerce.min
    val normalizedEcommerceMax = normalizedEcommerce.max

    val normalizedForeign = normalize.map(txn => txn(6))
    val normalizedForeignMean = normalizedForeign.mean
    val normalizedForeignStdev = normalizedForeign.stdev
    val normalizedForeignMin = normalizedForeign.min
    val normalizedForeignMax = normalizedForeign.max

    val normalizedNumberSameDay = normalize.map(txn => txn(7))
    val normalizedNumberSameDayMean = normalizedNumberSameDay.mean
    val normalizedNumberSameDayStdev = normalizedNumberSameDay.stdev
    val normalizedNumberSameDayMin = normalizedNumberSameDay.min
    val normalizedNumberSameDayMax = normalizedNumberSameDay.max

    println("### LEGAL TXNS ###")
    println("amount mean " + legalAmountMean + ", stdev: " + legalAmountStdev + " max:" + legalAmountMax + " min:" + legalAmountMin)
    println("amountOverMonth mean: " + legalAmountOverMonthMean + ", stdev: " + legalAmountOverMonthStdev + " max:" + legalAmountOverMonthMax + " min:" + legalAmountOverMonthMin)
    println("amountSameDay mean: " + legalAmountSameDayMean + ", stdev: " + legalAmountSameDayStdev + " max:" + legalAmountSameDayMax + " min:" + legalAmountSameDayMin)
    println("averageDailyOverMonth mean: " + legalAverageDailyOverMonthMean + ", stdev: " + legalAverageDailyOverMonthStdev + " max:" + legalAverageDailyOverMonthMax + " min:" + legalAverageDailyOverMonthMin)
    println("averageOverThreeMonths mean: " + legalAverageOverThreeMonthsMean + ", stdev: " + legalAverageOverThreeMonthsStdev + " max:" + legalAverageOverThreeMonthsMax + " min:" + legalAverageOverThreeMonthsMin)
    println("ecommerce mean: " + legalEcommerceMean + ", stdev: " + legalEcommerceStdev + " max:" + legalEcommerceMax + " min:" + legalEcommerceMin)
    println("foreign mean: " + legalForeignMean + ", stdev: " + legalForeignStdev + " max:" + legalForeignMax + " min:" + legalForeignMin)
    println("numberSameDay mean: " + legalNumberSameDayMean + ", stdev: " + legalNumberSameDayStdev + " max:" + legalNumberSameDayMax + " min:" + legalNumberSameDayMin)

    println("### FRAUD TXNS ###")
    println("amount mean " + fraudAmountMean + ", stdev: " + fraudAmountStdev + " max:" + fraudAmountMax + " min:" + fraudAmountMin)
    println("amountOverMonth mean: " + fraudAmountOverMonthMean + ", stdev: " + fraudAmountOverMonthStdev + " max:" + fraudAmountOverMonthMax + " min:" + fraudAmountOverMonthMin)
    println("amountSameDay mean: " + fraudAmountSameDayMean + ", stdev: " + fraudAmountSameDayStdev + " max:" + fraudAmountSameDayMax + " min:" + fraudAmountSameDayMin)
    println("averageDailyOverMonth mean: " + fraudAverageDailyOverMonthMean + ", stdev: " + fraudAverageDailyOverMonthStdev + " max:" + fraudAverageDailyOverMonthMax + " min:" + fraudAverageDailyOverMonthMin)
    println("averageOverThreeMonths mean: " + fraudAverageOverThreeMonthsMean + ", stdev: " + fraudAverageOverThreeMonthsStdev + " max:" + fraudAverageOverThreeMonthsMax + " min:" + fraudAverageOverThreeMonthsMin)
    println("ecommerce mean: " + fraudEcommerceMean + ", stdev: " + fraudEcommerceStdev + " max:" + fraudEcommerceMax + " min:" + fraudEcommerceMin)
    println("foreign mean: " + fraudForeignMean + ", stdev: " + fraudForeignStdev + " max:" + fraudForeignMax + " min:" + fraudForeignMin)
    println("numberSameDay mean: " + fraudNumberSameDayMean + ", stdev: " + fraudNumberSameDayStdev + " max:" + fraudNumberSameDayMax + " min:" + fraudNumberSameDayMin)

    println("### NORMALIZED DATA ###")
    println("amount mean " + normalizedAmountMean + ", stdev: " + normalizedAmountStdev + " max:" + normalizedAmountMax + " min:" + normalizedAmountMin)
    println("amountOverMonth mean: " + normalizedAmountOverMonthMean + ", stdev: " + normalizedAmountOverMonthStdev + " max:" + normalizedAmountOverMonthMax + " min:" + normalizedAmountOverMonthMin)
    println("amountSameDay mean: " + normalizedAmountSameDayMean + ", stdev: " + normalizedAmountSameDayStdev + " max:" + normalizedAmountSameDayMax + " min:" + normalizedAmountSameDayMin)
    println("averageDailyOverMonth mean: " + normalizedAverageDailyOverMonthMean + ", stdev: " + normalizedAverageDailyOverMonthStdev + " max:" + normalizedAverageDailyOverMonthMax + " min:" + normalizedAverageDailyOverMonthMin)
    println("averageOverThreeMonths mean: " + normalizedAverageOverThreeMonthsMean + ", stdev: " + normalizedAverageOverThreeMonthsStdev + " max:" + normalizedAverageOverThreeMonthsMax + " min:" + normalizedAverageOverThreeMonthsMin)
    println("ecommerce mean: " + normalizedEcommerceMean + ", stdev: " + normalizedEcommerceStdev + " max:" + normalizedEcommerceMax + " min:" + normalizedEcommerceMin)
    println("foreign mean: " + normalizedForeignMean + ", stdev: " + normalizedForeignStdev + " max:" + normalizedForeignMax + " min:" + normalizedForeignMin)
    println("numberSameDay mean: " + normalizedNumberSameDayMean + ", stdev: " + normalizedNumberSameDayStdev + " max:" + normalizedNumberSameDayMax + " min:" + normalizedNumberSameDayMin)


    // STOP SPARK CONTEXT
    sc.stop
  }
}