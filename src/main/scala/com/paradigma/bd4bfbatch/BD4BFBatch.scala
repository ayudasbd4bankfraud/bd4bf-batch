package com.paradigma.bd4bfbatch

import com.paradigma.bd4bfbatch.load.LoadManager
import com.paradigma.bd4bfbatch.statistics.StatisticsManager
import com.paradigma.bd4bfbatch.train.TrainManager

/**
  * Created by paradigma
  */
object BD4BFBatch extends {

  def main(args: Array[String]) {
    val mode: String = args(0)
    mode match {
      case "LOAD" => {
        println("Loading transaction data...")
        LoadManager.load(args(1))
        println("Data successfully loaded")
      }
      case "TRAIN" =>
        println("Training model...")
        TrainManager.train()
        println("Model successfully trained")
      case "STATISTICS" =>
        println("Generating statistics...")
        StatisticsManager.statistics()
        println("Statistics successfully generated")
      case _ => println("Unknown mode, exiting...")
    }
  }

}