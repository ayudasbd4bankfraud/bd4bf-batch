package com.paradigma.bd4bfbatch.domain

import scala.collection.mutable

/**
  * Created by paradigma
  */
case class LoadStatistics(
                           date: Long,
                           filename: String,
                           all: Map[String, mutable.Map[String, String]],
                           fraud: Map[String, mutable.Map[String, String]],
                           legal: Map[String, mutable.Map[String, String]]
                         ) extends Serializable {}