package com.paradigma.bd4bfbatch.domain

/**
 * Created by paradigma
 */
case class BD4BFStandardScaler(date: Long,
                               std: List[Double],
                               mean: List[Double]) {}