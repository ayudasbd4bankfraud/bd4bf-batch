package com.paradigma.bd4bfbatch.domain

/**
 * Created by paradigma
 */
case class BD4BFModel(date: Long,
                      weights: List[Double],
                      intercept: Double,
                      numFeatures: Int,
                      numClasses: Int) {}