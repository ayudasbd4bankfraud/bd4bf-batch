package com.paradigma.bd4bfbatch.load

import java.nio.file.Paths.get
import java.time.{Instant, LocalDateTime, ZoneOffset}
import java.util.TimeZone

import com.datastax.spark.connector.rdd.CassandraTableScanRDD
import com.datastax.spark.connector.{SomeColumns, _}
import com.paradigma.aggregations.Aggregations
import com.paradigma.aggregations.domain.{DailyAggregation, MonthlyAggregation, Transaction, TrimonthlyAggregation}
import com.paradigma.anonymizer.PartialAnonymization
import com.paradigma.bd4bfbatch.domain.{LoadStatistics, TransactionFeatures}
import com.paradigma.bd4bfbatch.util.BD4BFBatchUtils
import org.apache.log4j.{Level, Logger}
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.mutable

/**
  * Created by paradigma
  */
object LoadManager {

  private val errorTxns = scala.collection.mutable.MutableList.empty[String]

  def load(path: String): Unit = {

    Logger.getLogger("org").setLevel(Level.ERROR)

    val sc: SparkContext = new SparkContext(new SparkConf()
      .setMaster(BD4BFBatchUtils.master)
      .setAppName(BD4BFBatchUtils.appName)
      .set("spark.app.id", BD4BFBatchUtils.appId)
      .set("spark.cassandra.connection.host", BD4BFBatchUtils.cassandraHost))

    // remove header from RDD
    val data: RDD[String] = sc.textFile(path)
    val header: RDD[String] = sc.parallelize(data.take(1))
    val dataWithoutHeader: RDD[Array[String]] = data.subtract(header).map(line => line.split(","))

    // separate valid and invalid transactions
    val validTransactions: RDD[Array[String]] = dataWithoutHeader.filter(txn => isValidTxn(txn))
    val invalidTransactions: RDD[Array[String]] = dataWithoutHeader.filter(txn => !isValidTxn(txn))

    // take only positive amount transactions
    val validPositiveTransactions: RDD[Array[String]] = validTransactions.filter(txn => txn(4).toDouble > 0.0)
    val headerArray = header.collect()
    val anonymizedValidTransactions: RDD[Transaction] = validPositiveTransactions
      .map(txn => fromArrayToTransaction(anonymizeTransaction(headerArray, txn)))
      //.map(txn => fromArrayToTransaction(txn))

    val loadTxnsMinDate = anonymizedValidTransactions.map(txn => txn.date).min

    val transactionsFromCassandra: CassandraTableScanRDD[Transaction] =
      sc.cassandraTable[Transaction](
        BD4BFBatchUtils.cassandraKeyspace,
        BD4BFBatchUtils.cassandraAnonTable
      ).where("date >= ?", loadTxnsMinDate)

    val allTxnsFromCassandraByCreditCard: RDD[(String, Iterable[Transaction])] =
      transactionsFromCassandra.keyBy(txn => txn.ccnumber).groupByKey
    val anonymizedValidTransactionsByCreditCard: RDD[(String, Iterable[Transaction])] =
      anonymizedValidTransactions.keyBy(txn => txn.ccnumber).groupByKey

    val transactionsWithFeatures: RDD[TransactionFeatures] =
      anonymizedValidTransactionsByCreditCard.leftOuterJoin(allTxnsFromCassandraByCreditCard)
        .flatMap {
          case (_, (txnsFromCsv, txnsFromCassandra)) =>
            txnsFromCsv.map(transaction =>
              (transaction, Seq.concat(txnsFromCsv.toSeq, txnsFromCassandra match {
                case Some(iterable) => iterable.toSeq
                case None => Seq.empty
              }))
            )
        }.map({ case (txn, relatedTransactions) => aggregateTransaction(txn, relatedTransactions) })

    // save anonymized transactions
    anonymizedValidTransactions.
      saveToCassandra(
        BD4BFBatchUtils.cassandraKeyspace,
        BD4BFBatchUtils.cassandraAnonTable,
        BD4BFBatchUtils.anonTxnColumnSelector
        )

    // save aggregated transactions
    transactionsWithFeatures
      .saveToCassandra(
        BD4BFBatchUtils.cassandraKeyspace,
        BD4BFBatchUtils.cassandraFeatureTable,
        BD4BFBatchUtils.featTxnColumnSelector
        )

    // print file with failed transactions
    sc.parallelize(errorTxns).repartition(1)
      .saveAsTextFile(BD4BFBatchUtils.errorTxnLogFolder + System.currentTimeMillis / 1000 + BD4BFBatchUtils.errorTxnLogSuffix)

    // print file with invalid transactions
    invalidTransactions.
      map(txn => txn.mkString("|")).repartition(1)
      .saveAsTextFile(BD4BFBatchUtils.errorTxnLogFolder + System.currentTimeMillis / 1000 + BD4BFBatchUtils.invalidTxnLogSuffix)

    // calculate and save statistics of each load process
    loadStatistics(validPositiveTransactions.map(txn => fromArrayToTransaction(txn)), sc, path)

    // STOP SPARK CONTEXT
    sc.stop
  }

  private def isValidTxn(txn: Array[String]): Boolean = {
    txn.nonEmpty && txn.forall(value => value.nonEmpty)
  }

  // remove comments to anonymize transactions
    private def anonymizeTransaction(header: Array[String], txn: Array[String]): Array[String] = {
      var anonymizedTxn: Array[String] = Array.empty[String]
      try {
        anonymizedTxn = PartialAnonymization.anonymize(header, txn, BD4BFBatchUtils.FieldsToAnonymize)
      } catch {
        case iae: IllegalArgumentException =>
          errorTxns.addString(new StringBuilder("Error anonymizing transaction (" + iae.getMessage + "): " + txn.mkString("|")))
          println("Error anonymizing transaction (" + iae.getMessage + ") ", txn.mkString("|"))
        case e: Exception =>
          errorTxns.addString(new StringBuilder("Unknown error anonymizing transaction (" + e.getMessage + "): " + txn.mkString("|")))
          println("Unknown error anonymizing transaction (" + e.getMessage + ") ", txn.mkString("|"))
      }
      anonymizedTxn
    }

  private def aggregateTransaction(txn: Transaction, validTransactions: Seq[Transaction]): TransactionFeatures = {
    val endTimestamp: Long = txn.date
    val endDate: LocalDateTime = LocalDateTime.ofInstant(Instant.ofEpochSecond(endTimestamp), TimeZone.getDefault.toZoneId)

    // three months aggregations
    val threeMonthStart: Long = endDate.minusMonths(3).toEpochSecond(ZoneOffset.UTC)
    val threeMonthTxns: Seq[Transaction] =
      validTransactions.filter(txn => txn.date >= threeMonthStart && txn.date <= endTimestamp)
    val threeMonthAggs: TrimonthlyAggregation = Aggregations.calculateTrimonthlyAggregations(threeMonthTxns)

    // one month aggregations
    val oneMonthStart: Long = endDate.minusMonths(1).toEpochSecond(ZoneOffset.UTC)
    val oneMonthTxns: Seq[Transaction] = threeMonthTxns.filter(txn => txn.date >= oneMonthStart && txn.date <= endTimestamp)
    val oneMonthAggs: MonthlyAggregation = Aggregations.calculateMonthlyAggregations(oneMonthTxns)

    // one day aggregations
    val oneDayStart: Long = endDate.minusHours(24).toEpochSecond(ZoneOffset.UTC)
    val oneDayTxns: Seq[Transaction] = oneMonthTxns.filter(txn => txn.date >= oneDayStart && txn.date <= endTimestamp)
    val oneDayAggs: DailyAggregation = Aggregations.calculateDailyAggregations(oneDayTxns)

    transactionFeaturesBuilder(txn, oneDayAggs, oneMonthAggs, threeMonthAggs)
  }

  private def transactionFeaturesBuilder(txn: Transaction, dailyAggregation: DailyAggregation,
                                         monthlyAggregation: MonthlyAggregation,
                                         trimonthlyAggregation: TrimonthlyAggregation): TransactionFeatures = {
    new TransactionFeatures(txn.ccnumber, txn.txnid, txn.amount, txn.date, txn.ecommerce, txn.foreign, txn.fraud, txn.merchant, txn.country,
      dailyAggregation.amountSameDay, dailyAggregation.numberSameDay, monthlyAggregation.amountOverMonth,
      monthlyAggregation.averageDailyOverMonth, monthlyAggregation.amountMerchantTypeOverMonth,
      monthlyAggregation.numberMerchantTypeOverMonth, monthlyAggregation.amountSameCountryOverMonth, monthlyAggregation.numberSameCountryOverMonth,
      trimonthlyAggregation.averageOverThreeMonths, trimonthlyAggregation.amountMerchantTypeOverThreeMonths)
  }

  private def fromArrayToTransaction(txn: Array[String]): Transaction = {
    // array[ ccnumber, date, merchant, foreign, amount, ecommerce, country, txnid, fraud ]
    new Transaction(txn(0), txn(7), txn(4).toDouble, txn(1).toLong, txn(5).toInt, txn(3).toInt, txn(8).toInt, txn(2), txn(6))
  }

  private def loadStatistics(transactions: RDD[Transaction], sc: SparkContext, path: String): Unit = {
    if (!transactions.isEmpty) {
      val all: RDD[Transaction] = transactions.cache
      val legal: RDD[Transaction] = all.filter(txn => txn.fraud == 0).cache
      val fraud: RDD[Transaction] = all.filter(txn => txn.fraud == 1).cache

      val allStats = calculateStats(all)
      val fraudStats = calculateStats(fraud)
      val legalStats = calculateStats(legal)

      val p = get(path)
      val filename = p.getFileName.toString


      sc.parallelize(Seq(new LoadStatistics(System.currentTimeMillis, filename,
        allStats.toMap, fraudStats.toMap, legalStats.toMap)))
        .saveToCassandra(
          "bd4bf",
          "load_statistics",
          SomeColumns("date", "filename", "all", "fraud", "legal" +
            ""))
    } else {
      println("No transactions for statistics calculation!")
    }
  }

  private def calculateStats(rdd: RDD[Transaction]): mutable.Map[String, mutable.Map[String, String]] = {
    val amounts = rdd.map(txn => txn.amount)
    val stats = initStatsMap()
    stats.getOrElse(BD4BFBatchUtils.Count, mutable.Map[String, String]()).update(BD4BFBatchUtils.Value, rdd.count.toString)
    stats.getOrElse(BD4BFBatchUtils.SumAmount, mutable.Map[String, String]()).update(BD4BFBatchUtils.Value, amounts.sum.toString)
    stats.getOrElse(BD4BFBatchUtils.MeanAmount, mutable.Map[String, String]()).update(BD4BFBatchUtils.Value, amounts.mean.toString)
    stats.getOrElse(BD4BFBatchUtils.StdevAmount, mutable.Map[String, String]()).update(BD4BFBatchUtils.Value, if (!amounts.stdev.isNaN) amounts.stdev.toString else "0.0")
    aggregateByKey(rdd, stats)
    stats
  }

  private def aggregateByKey(rdd: RDD[Transaction], statistics: => mutable.Map[String, mutable.Map[String, String]]): Unit = {

    // SUM
    rdd.map(txn => (txn.country, txn.amount)).reduceByKeyLocally(_ + _)
      .foreach({ case (country, sum) =>
        statistics.getOrElse(BD4BFBatchUtils.AmountPerCountry, mutable.Map[String, String]()).put(country, sum.toString)
      })
    rdd.map(txn => (txn.ecommerce.toString, txn.amount)).reduceByKeyLocally(_ + _)
      .foreach({ case (ecommerce, sum) =>
        statistics.getOrElse(BD4BFBatchUtils.AmountPerEcommerce, mutable.Map[String, String]()).put(ecommerce, sum.toString)
      })
    rdd.map(txn => (txn.foreign.toString, txn.amount)).reduceByKeyLocally(_ + _)
      .foreach({ case (foreign, sum) =>
        statistics.getOrElse(BD4BFBatchUtils.AmountPerForeign, mutable.Map[String, String]()).put(foreign, sum.toString)
      })
    rdd.map(txn => (txn.merchant, txn.amount)).reduceByKeyLocally(_ + _)
      .foreach({ case (merchant, sum) =>
        statistics.getOrElse(BD4BFBatchUtils.AmountPerMerchant, mutable.Map[String, String]()).put(merchant, sum.toString)
      })

    // COUNT
    rdd.map(txn => (txn.country, 1)).countByKey
      .foreach({ case (country, count) =>
        statistics.getOrElse(BD4BFBatchUtils.CountPerCountry, mutable.Map[String, String]()).put(country, count.toString)
      })
    rdd.map(txn => (txn.ecommerce.toString, 1)).countByKey
      .foreach({ case (ecommerce, count) =>
        statistics.getOrElse(BD4BFBatchUtils.CountPerEcommerce, mutable.Map[String, String]()).put(ecommerce, count.toString)
      })
    rdd.map(txn => (txn.foreign.toString, 1)).countByKey
      .foreach({ case (foreign, count) =>
        statistics.getOrElse(BD4BFBatchUtils.CountPerForeign, mutable.Map[String, String]()).put(foreign.toString, count.toString)
      })
    rdd.map(txn => (txn.merchant, 1)).countByKey
      .foreach({ case (merchant, count) =>
        statistics.getOrElse(BD4BFBatchUtils.CountPerMerchant, mutable.Map[String, String]()).put(merchant, count.toString)
      })

  }

  private def initStatsMap(): collection.mutable.Map[String, collection.mutable.Map[String, String]] = {

    val value = mutable.Map[String, String]((BD4BFBatchUtils.Value, "0"))
    val zeroOne = mutable.Map[String, String](("0", "0"), ("1", "0"))

    val merchantCodes =
      mutable.Map[String, String](("0742", "0"), ("0763", "0"), ("1520", "0"), ("0780", "0"))
    val countries =
      mutable.Map[String, String](("FR", "0"), ("US", "0"), ("ES", "0"), ("GR", "0"), ("CH", "0"))


    val result: mutable.Map[String, mutable.Map[String, String]] =
      collection.mutable.Map.empty[String, collection.mutable.Map[String, String]]
    result.put(BD4BFBatchUtils.Count, value.clone)
    result.put(BD4BFBatchUtils.SumAmount, value.clone)
    result.put(BD4BFBatchUtils.MeanAmount, value.clone)
    result.put(BD4BFBatchUtils.StdevAmount, value.clone)
    result.put(BD4BFBatchUtils.CountPerCountry, countries.clone)
    result.put(BD4BFBatchUtils.CountPerEcommerce, zeroOne.clone)
    result.put(BD4BFBatchUtils.CountPerForeign, zeroOne.clone)
    result.put(BD4BFBatchUtils.CountPerMerchant, merchantCodes.clone)
    result.put(BD4BFBatchUtils.AmountPerCountry, countries.clone)
    result.put(BD4BFBatchUtils.AmountPerEcommerce, zeroOne.clone)
    result.put(BD4BFBatchUtils.AmountPerForeign, zeroOne.clone)
    result.put(BD4BFBatchUtils.AmountPerMerchant, merchantCodes.clone)
    result
  }

}
