package com.paradigma.bd4bfbatch.util

import com.datastax.spark.connector.{ColumnSelector, SomeColumns}
import com.paradigma.bd4bfbatch.domain.TransactionFeatures
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import org.apache.spark.rdd.RDD

/**
  * Created by paradigma
  */
object BD4BFBatchUtils {

  def Value = "value"
  def Count: String = "count"
  def SumAmount: String = "sumAmount"
  def MeanAmount: String = "meanAmount"
  def StdevAmount: String = "stdevAmount"
  def CountPerCountry: String = "countPerCountry"
  def CountPerEcommerce: String = "countPerEcommerce"
  def CountPerForeign: String = "countPerForeign"
  def CountPerMerchant: String = "countPerMerchant"
  def AmountPerCountry: String = "amountPerCountry"
  def AmountPerEcommerce: String = "amountPerEcommerce"
  def AmountPerForeign: String = "amountPerForeign"
  def AmountPerMerchant: String = "amountPerMerchant"
  def cassandraHost : String = "localhost"
  def cassandraKeyspace: String = "bd4bf"
  def cassandraAnonTable: String = "anonymized_txn_batch"
  def cassandraFeatureTable: String = "txn_features"
  def cassandraLRModelTable : String = "logistic_regression_model"
  def cassandraSSCModelTable: String = "standard_scaler_model"
  def anonTxnColumnSelector: ColumnSelector = SomeColumns("ccnumber", "txnid", "amount", "date", "ecommerce", "foreign", "fraud", "merchant", "country")
  def featTxnColumnSelector : ColumnSelector = SomeColumns("ccnumber", "txnid", "amount", "date", "ecommerce", "foreign", "fraud", "merchant", "country",
    "amount_same_day", "number_same_day", "amount_over_month", "average_daily_over_month",
    "amount_merchant_type_over_month", "number_merchant_type_over_month", "amount_same_country_over_month",
    "number_same_country_over_month", "average_over_three_months", "amount_merchant_type_over_three_months")
  def lrModelColumnSelector: ColumnSelector = SomeColumns("date", "num_classes", "num_features", "intercept", "weights")
  def sscModelColumnSelector: ColumnSelector = SomeColumns("date", "std", "mean")
  def errorTxnLogFolder : String = "/tmp/bd4bf_batch/error_txns/"
  def errorTxnLogSuffix : String = "_error_txns"
  def invalidTxnLogSuffix : String = "_invalid_txns"

  def FieldsToAnonymize: List[String] = List[String]("country")

  def master = "local[*]"
  def appName = "BD4BFBatchLoadManager"
  def appId = "BD4BFBatchLoadManager"
  def Merchants = List[String]("0742", "0763", "1520", "0780")
  def Countries = List[String]("FR", "US", "ES", "GB", "GR", "CH")
  def trainingIterations = 500
  def trainingStepSize = 100


  def fromRddToVector(rdd: RDD[TransactionFeatures]): RDD[(String, Int, Vector)] = {
    rdd.map(txn => (txn.txnid, txn.fraud, Vectors.dense(Array(
      txn.amount,
      txn.amountOverMonth,
      txn.amountSameDay,
      txn.averageDailyOverMonth,
      txn.amountMerchantTypeOverMonth(Merchants.head),
      txn.amountMerchantTypeOverMonth(Merchants(1)),
      txn.amountMerchantTypeOverMonth(Merchants(2)),
      txn.amountMerchantTypeOverMonth(Merchants(3)),
      txn.numberMerchantTypeOverMonth(Merchants.head).toDouble,
      txn.numberMerchantTypeOverMonth(Merchants(1)).toDouble,
      txn.numberMerchantTypeOverMonth(Merchants(2)).toDouble,
      txn.numberMerchantTypeOverMonth(Merchants(3)).toDouble,
      txn.amountSameCountryOverMonth(Countries.head),
      txn.amountSameCountryOverMonth(Countries(1)),
      txn.amountSameCountryOverMonth(Countries(2)),
      txn.amountSameCountryOverMonth(Countries(3)),
      txn.amountSameCountryOverMonth(Countries(4)),
      txn.amountSameCountryOverMonth(Countries(5)),
      txn.numberSameCountryOverMonth(Countries.head).toDouble,
      txn.numberSameCountryOverMonth(Countries(1)).toDouble,
      txn.numberSameCountryOverMonth(Countries(2)).toDouble,
      txn.numberSameCountryOverMonth(Countries(3)).toDouble,
      txn.numberSameCountryOverMonth(Countries(4)).toDouble,
      txn.numberSameCountryOverMonth(Countries(5)).toDouble,
      txn.averageOverThreeMonths,
      txn.ecommerce.toDouble,
      txn.foreign.toDouble,
      txn.numberSameDay.toDouble,
      txn.amountMerchantTypeOverThreeMonths(Merchants.head),
      txn.amountMerchantTypeOverThreeMonths(Merchants(1)),
      txn.amountMerchantTypeOverThreeMonths(Merchants(2)),
      txn.amountMerchantTypeOverThreeMonths(Merchants(3))
    ))))
  }

}
