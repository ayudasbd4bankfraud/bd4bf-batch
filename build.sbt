name := "bd4bf-batch"

organization := "com.paradigma.bd4bf"

version := "1.0.1"

scalaVersion in ThisBuild := "2.10.5"

parallelExecution in test := false

libraryDependencies ++= Seq(
  "org.scala-lang" % "scala-compiler" % "2.10.5",
  "org.scala-lang" % "scala-reflect" % "2.10.5",
  "org.apache.spark" % "spark-core_2.10" % "1.5.0",
  "org.apache.spark" % "spark-mllib_2.10" % "1.5.0",
  "org.scalatest" % "scalatest_2.10" % "2.0" % "test",
  "org.apache.commons" % "commons-lang3" % "3.4",
  "com.datastax.spark" % "spark-cassandra-connector_2.10" % "1.4.1",
  "com.paradigma" % "anonymizer_2.10" % "1.0.0",
  "com.paradigma.bd4bf" % "bd4bf-aggregations_2.10" % "1.0.1",
  "org.apache.hadoop" % "hadoop-client" % "2.6.0" % "compile,test" exclude("javax.servlet", "servlet-api"),
  "org.slf4j" % "slf4j-api" % "1.7.14"
)

assemblyMergeStrategy in assembly := {
  case "reference.conf" => MergeStrategy.concat
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case m if m.toLowerCase.matches("meta-inf/services/.*") => MergeStrategy.concat
  case m if m.toLowerCase.matches("meta-inf/.*\\.sf$") => MergeStrategy.discard
  case _ => MergeStrategy.first
}